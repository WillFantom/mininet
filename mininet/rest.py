"""
A simple REST interface for Mininet.

The Mininet REST API provides a simple way to control nodes when 
running mininet headless.
"""
from bottle import Bottle, HTTPResponse, request
import threading
import json
import os.path

from mininet.log import info, output, error

class REST( Bottle ):
    "Simple RESTful interface to talk to nodes."

    def __init__( self, mininet, *args, **kwargs ):
        """Start and run REST API for host interaction
           mininet: Mininet network object
           port: port to serve from"""
        super(REST, self).__init__()
        self.mn = mininet
        port = 8080
        thread = threading.Thread(target=self.run_server, args=(port,), daemon=True)
        thread.start()
        self.tracked_cmds = {}
        info( '*** Starting API\n' )

    def run_server( self, port ):
        self.route('/nodes', callback=self.get_nodes)
        self.route('/cmd/<node_name>', method=['POST'], callback=self.do_cmd)
        self.route('/cmdout/<cmd_name>', callback=self.do_cmdout)
        self.route('/cmdcheck/<cmd_name>', callback=self.do_cmdcheck)
        self.route('/cmdint/<cmd_id>', callback=self.do_cmdint)
        self.route('/cmdkill/<cmd_id>', callback=self.do_cmdkill)
        try:
            self.run(host='0.0.0.0', port=port, quiet=True)
        except Exception as e:
            print( e )
            error( 'Failed to start REST API\n' )

    def build_response( self, data, code=200 ):
        if not isinstance(data, dict):
            data = json.dumps({"error": "could not create json response"})
            return HTTPResponse(status=500, body=data)
        return HTTPResponse(status=code, body=data)

    def read_req_body( self, body ):
        try:
            return json.loads(body.read().decode('utf-8'))
        except Exception as e:
            return None

    def replace_names( self, cmd_str ):
        args = cmd_str.split(' ')
        args = [ self.mn[ arg ].defaultIntf().updateIP() or arg
                     if arg in self.mn else arg
                     for arg in args ]
        return ' '.join( args )

    def get_nodes( self ):
        """Get a list of nodes that exist within a topo"""
        data = {"nodes": [node for node in self.mn]}
        return self.build_response(data)

    def do_cmd( self, node_name ):
        """Run a command on a given host"""
        if not node_name in self.mn:
            data = {"error": "node {} does not exist".format(node_name)}
            return self.build_response(data, code=400)
        body = self.read_req_body(request.body)
        if body:
            if "command" in body and "name" in body:
                command = self.replace_names(body.get("command"))
                file_name = "/tmp/{}.mncmd".format(body.get("name"))
                if os.path.isfile(file_name) or body.get("name") in self.tracked_cmds:
                    data = {"error": "command with name {} exists".format(body.get("name"))}
                    return self.build_response(data, code=500)
                try:
                    pipe_file = open(file_name, 'x')
                except:
                    data = {"error": "could not create file to use as std out"}
                    return self.build_response(data, code=500)
                popen = self.mn[ node_name ].popen( command, stdout=pipe_file, stderr=pipe_file )
                command_track = {
                    "process": popen,
                    "out_file": file_name,
                    "cmd_node": node_name
                }
                self.tracked_cmds[body.get("name")] = command_track
                data = {"pid": popen.pid}
                return self.build_response(data)
        data = {"error": "usage: command (str) name (str)"}
        return self.build_response(data, code=400)

    def do_cmdcheck( self, cmd_name ):
        """Check the running status of of command"""
        if cmd_name in self.tracked_cmds:
            process = self.tracked_cmds.get(cmd_name).get("process")
            process.poll()
            data = {
                "return_code": process.returncode,
                "pid": process.pid
            }
            return self.build_response(data)
        data = {"error": "no command of that name found"}
        return self.build_response(data, code=400)

    def do_cmdsig( self, pid, sig ):
        try:
            os.kill(pid, sig)
            data = {"success": ""}
            return self.build_response(data)
        except:
            data = {"error": "could not send signal to process id: {}".format(str(pid))}
            return self.build_response(data, code=500)

    def do_cmdint( self, cmd_id ):
        """Interrupt command on a host"""
        if isinstance(cmd_id, int):
            # Assume is pid
            return self.do_cmdsig(cmd_id, 2)
        else:
            if cmd_id in self.tracked_cmds:
                return self.do_cmdsig(self.tracked_cmds.get(cmd_id).get("process").pid, 2)
        data = {"error": "command not found"}
        return self.build_response(data, code=400)
    
    def do_cmdkill( self, cmd_id ):
        """Kill command on a host"""
        if isinstance(cmd_id, int):
            # Assume is pid
            return self.do_cmdsig(cmd_id, 9)
        else:
            if cmd_id in self.tracked_cmds:
                return self.do_cmdsig(self.tracked_cmds.get(cmd_id).get("process").pid, 9)
        data = {"error": "command not found"}
        return self.build_response(data, code=400)

    def do_cmdout( self, cmd_name ):
        """Get the output of a cmd"""
        if not cmd_name in self.tracked_cmds:
            data = {"error": "command with name {} does not exist".format(cmd_name)}
            return self.build_response(data, code=400) 
        file_name = self.tracked_cmds.get(cmd_name).get("out_file")
        if not os.path.isfile(file_name):
            data = {"error": "output file for cmd name {} can not be read".format(cmd_name)}
            return self.build_response(data, code=500) 
        try:
            with open(file_name, 'r') as out_file:
                output = out_file.read()
            data = {"output": str(output)}
            return self.build_response(data)
        except:
            data = {"error": "could not read output file"}
            return self.build_response(data, code=500)
